#!/usr/bin/env bash

sudo /root/.cargo/bin/agate --content /root/gemini/content/ \
      --addr [::]:1965 \
      --addr 0.0.0.0:1965 \
      --hostname gem.zaney.org \
      --lang en-US
